import 'package:dio/dio.dart';

class ProjectClient {
  static BaseOptions _options = new BaseOptions(
    baseUrl: "http://salemanagerapi.xyz",
    connectTimeout: 5000,
    receiveTimeout: 3000,

  );
  static Dio _dio = Dio(_options);

  ProjectClient._internal(){
    _dio.interceptors.add(LogInterceptor(responseBody: true));
  }
  static final ProjectClient instance = ProjectClient._internal();

  Dio get dio=> _dio;
}