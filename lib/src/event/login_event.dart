import 'package:app_salemanager_project/src/base_project/base_event.dart';
import 'package:flutter/cupertino.dart';
class LoginEvent extends BaseEvent {
 final String username;
  final String password;

  LoginEvent({@required this.username, @required this.password});
 
}

