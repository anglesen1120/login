import 'dart:async';
import 'package:app_salemanager_project/src/shared/helpper/validate.dart';
import 'package:rxdart/rxdart.dart';

import 'package:app_salemanager_project/src/base_project/base_bloc.dart';
import 'package:app_salemanager_project/src/base_project/base_event.dart';
import 'package:app_salemanager_project/src/data/repository/user_repo.dart';
import 'package:app_salemanager_project/src/event/login_event.dart';
import 'package:flutter/material.dart';

class LoginBloc extends BaseBloc {
  final _salemancodeSubject = BehaviorSubject<String>();
  final _passwordSubject = BehaviorSubject<String>();
  final _btnSubject = BehaviorSubject<bool>();

  UserRepository _userRepository;
  LoginBloc({@required UserRepository userRepository}) {
    _userRepository = userRepository;
    validateForm();
  }

  var salesmancodeValidation = StreamTransformer<String, String>.fromHandlers(
      handleData: (salesmancode, sink) {
    if (Validation.validateSalesmanCode(salesmancode)) {
      sink.add(null);
      return;
    }
    sink.add('Salesman Code is valid');
  });

  var passwordValidation = StreamTransformer<String, String>.fromHandlers(
      handleData: (password, sink) {
    if (Validation.validatePass(password)) {
      sink.add(null);
      return;
    }
    sink.add('Password is validate');
  });

  Stream<String> get salessmancodeStream =>
      _salemancodeSubject.stream.transform(salesmancodeValidation);
  Sink<String> get salesmanSink => _salemancodeSubject.sink;

  Stream<String> get passwordStream =>
      _passwordSubject.stream.transform(passwordValidation);
  Sink<String> get passwordSink => _passwordSubject.sink;


  Stream<bool> get btnStream =>_btnSubject.stream;
  Sink<bool> get btnSink => _btnSubject.sink;

  validateForm(){
    Observable.combineLatest2(_salemancodeSubject, _passwordSubject, (salesmancode, password){
      return Validation.validateSalesmanCode(salesmancode) && Validation.validatePass(password);
    },).listen((enable){
      btnSink.add(enable);
    });
  }

  @override
  void dispatchEvent(BaseEvent event) {
    switch (event.runtimeType) {
      case LoginEvent:
        handleLogin(event);
        break;
      default:
    }
  }

  handleLogin(event) {
    LoginEvent e = event as LoginEvent;
    _userRepository.logIn(e.username, e.password).then(
      (value) {
        print(value);
      },
      onError: (e) {
        print(e);
      },
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _passwordSubject.close();
    _salemancodeSubject.close();
    _btnSubject.close();
  }
}
