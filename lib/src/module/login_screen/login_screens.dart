import 'package:app_salemanager_project/src/base_project/base_widget.dart';
import 'package:app_salemanager_project/src/data/remote/user_services.dart';
import 'package:app_salemanager_project/src/data/repository/user_repo.dart';
import 'package:app_salemanager_project/src/event/login_event.dart';
import 'package:app_salemanager_project/src/module/login_screen/login_bloc.dart';
import 'package:app_salemanager_project/src/shared/widget_customer/custom_waveclipper_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return PageContainer(
      di: [
        Provider.value(
          value: UserServices(),
        ),
        ProxyProvider<UserServices, UserRepository>(
          builder: (context, userServices, previous) =>
              UserRepository(userServices: userServices),
        ),
      ],
      providers: [],
      child: LoginForm(),
    );
  }
}

class LoginForm extends StatelessWidget {
  final TextEditingController txtPasswordController = TextEditingController();
  final TextEditingController txtUsernameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Provider<LoginBloc>.value(
      value: LoginBloc(userRepository: Provider.of(context)),
      child: Consumer<LoginBloc>(
        builder: (context, bloc, child)=> ListView(
          children: <Widget>[
            _buildTitle(),
            SizedBox(
              height: 30,
            ),
            _buildSalesmanCodeField(bloc),
            SizedBox(
              height: 20,
            ),
            _buildPassField(bloc),
            SizedBox(
              height: 25,
            ),
            _buildButtonLogin(bloc),
            SizedBox(
              height: 20,
            ),
            _buildFooterLogin(),
          ],
        ),
      ),
    );
  }

  Widget _buildTitle(){
  return Stack(
            children: <Widget>[
              ClipPath(
                clipper: Two_WaveClipper(),
                child: Container(
                  child: Column(),
                  width: double.infinity,
                  height: 300,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [Color(0x22ff3a5a), Color(0x22fe494d)])),
                ),
              ),
              ClipPath(
                clipper: WaveClipper(),
                child: Container(
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 40,
                      ),
                      Icon(
                        Icons.fastfood,
                        color: Colors.white,
                        size: 60,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        "Login System",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                            fontSize: 30),
                      ),
                    ],
                  ),
                  width: double.infinity,
                  height: 300,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [Color(0xffff3a5a), Color(0xfffe494d)])),
                ),
              ),
            ],
          );
  }




  Widget _buildFooterLogin() {
    return Center(
      child: Text(
        "Forget Password",
        style: TextStyle(
            color: Colors.red, fontSize: 12, fontWeight: FontWeight.w700),
      ),
    );
  }

  /// Widget TextFeild SalemanCode
  Widget _buildSalesmanCodeField(LoginBloc bloc) {
    return StreamProvider<String>.value(
      initialData: null,
      value: bloc.salessmancodeStream,
       child:Consumer<String>(
         builder: (context, msg, child)=> Padding(
          padding: EdgeInsets.symmetric(horizontal: 32),
          child: Material(
          elevation: 0.0,
          borderRadius: BorderRadius.all(Radius.circular((10))),
          child: TextField(
            controller: txtUsernameController,
            onChanged: (value) {bloc.salesmanSink.add(value);},
            cursorColor: Colors.deepOrange,
            decoration: InputDecoration(
                hintText: "Salesman Code",
                prefixIcon: Material(
                  elevation: 0,
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  child: Icon(
                    Icons.email,
                    color: Colors.red,
                  ),
                ),
                border: InputBorder.none,
                errorText: msg,
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 25, vertical: 13)),
          ),
      ),
    ),
       ),

    );
    
  }

  /// Widget Text Field Password
  Widget _buildPassField(LoginBloc bloc) {
    return StreamProvider<String>.value(
      initialData: null,
      value: bloc.passwordStream,
      child: Consumer<String>(
        builder: (context, msg, child) =>Padding(
      padding: EdgeInsets.symmetric(horizontal: 32),
      child: Material(
        elevation: 0.0,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        child: TextField(
          controller: txtPasswordController,
          obscureText: true,
          onChanged: (value) {bloc.passwordSink.add(value);},
          cursorColor: Colors.deepOrange,
          decoration: InputDecoration(
              hintText: "Password",
              prefixIcon: Material(
                elevation: 0.0,
                borderRadius: BorderRadius.all(Radius.circular(10)),
                child: Icon(
                  Icons.lock,
                  color: Colors.red,
                ),
              ),
              errorText: msg,
              border: InputBorder.none,
              contentPadding:
                  EdgeInsets.symmetric(horizontal: 25, vertical: 13)),
        ),
      ),
    ),
      ),
    );
    
  }

  Widget _buildButtonLogin(LoginBloc bloc) {
    return StreamProvider<bool>.value(
      initialData: false,
      value: bloc.btnStream,
      child: Consumer<bool>(
        builder: (context, enable, child)=>Padding(
      padding: EdgeInsets.symmetric(horizontal: 32),
      
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(100)),
              color: Color(0xffff3a5a)),
          child: FlatButton(
              onPressed:enable ?() {
                bloc.event.add(LoginEvent(
                  username: txtUsernameController.text,
                  password: txtPasswordController.text,
                ),);
              } : null,
              child: Text(
                "Login",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                    fontSize: 18),
              )),
        ),
      
    ),

      ),
    );
    
  }

}

