class Validation {
  static  validatePass(String pass) {
    return pass.length > 4;
  }


  static  validateSalesmanCode (String salesmancode){
   
    final isValid = RegExp(r"^[a-zA-Z0-9]+");
    return isValid.hasMatch(salesmancode);
   

  

  }
}
