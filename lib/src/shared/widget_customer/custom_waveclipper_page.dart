import 'package:flutter/material.dart';

class WaveClipper extends CustomClipper<Path>{
  @override
  Path getClip(Size size){
    final path = Path();
    path.lineTo(0.0, size.height - 50);

    var firstEndPoint = Offset(size.width * 0.6, size.height - 29-50);
    var firstControlPoint = Offset(size.width*.25, size.height -60-50);
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy, firstEndPoint.dx, firstEndPoint.dy);

    var seconEndPoint = Offset(size.width, size.height-60);
    var serconControllPoint = Offset(size.width * 0.84 , size.height - 50);
    path.quadraticBezierTo(serconControllPoint.dx, serconControllPoint.dy, seconEndPoint.dx, seconEndPoint.dy);


    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);
    path.close();
    return path;

  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;

}


class Two_WaveClipper extends CustomClipper<Path>{
  @override
  Path getClip(Size size){
    final path = Path();
    path.lineTo(0.0, size.height - 50);

    var firstEndPoint = Offset(size.width * 7, size.height - 40);
    var firstControlPoint = Offset(size.width*.25, size.height);
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy, firstEndPoint.dx, firstEndPoint.dy);

    var seconEndPoint = Offset(size.width, size.height-45);
    var serconControllPoint = Offset(size.width * 0.84 , size.height - 50);
    path.quadraticBezierTo(serconControllPoint.dx, serconControllPoint.dy, seconEndPoint.dx, seconEndPoint.dy);


    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);
    path.close();
    return path;

  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;

}


class Three_WaveClipper extends CustomClipper<Path>{
  @override
  Path getClip(Size size){
    final path = Path();
    path.lineTo(0.0, size.height - 50);

    var firstEndPoint = Offset(size.width * 0.6, size.height - 15- 50);
    var firstControlPoint = Offset(size.width*.25, size.height - 60 -50);
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy, firstEndPoint.dx, firstEndPoint.dy);

    var seconEndPoint = Offset(size.width, size.height-40);
    var serconControllPoint = Offset(size.width * 0.84 , size.height - 30);
    path.quadraticBezierTo(serconControllPoint.dx, serconControllPoint.dy, seconEndPoint.dx, seconEndPoint.dy);


    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);
    path.close();
    return path;

  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;

}


