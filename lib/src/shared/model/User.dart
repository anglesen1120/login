class User {
  String token;

  User({this.token});

  factory User.fromJson(Map<String, dynamic> map){
    return User(token: map['Token']);

  }
  
}