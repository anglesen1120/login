import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PageContainer extends StatelessWidget {
  final String title;
  final Widget child;


  final List<SingleChildCloneableWidget> providers;
  final List<SingleChildCloneableWidget> di;

  PageContainer({this.title, this.child, this.providers, this.di});

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
        body: MultiProvider(
        providers: [
          ...di,
          ...providers,
        ],
        child: child,
      ),
    );
  }
}
