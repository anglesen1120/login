import 'dart:async';
import 'package:flutter/material.dart';

import 'package:app_salemanager_project/src/base_project/base_event.dart';


abstract class BaseBloc {
  StreamController<BaseEvent> _eventController = StreamController<BaseEvent>();

  Sink<BaseEvent> get event => _eventController.sink;

  BaseBloc(){
    _eventController.stream.listen((event) {
      if(event is! BaseEvent){
        throw Exception("Invalid Event");
      }
      dispatchEvent(event);
    });
  }


  void dispatchEvent(BaseEvent event);

  @mustCallSuper
  void dispose(){
    _eventController.close();
  }
}