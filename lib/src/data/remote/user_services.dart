import 'package:app_salemanager_project/src/networks/project_client.dart';
import 'package:dio/dio.dart';

class UserServices {
  Future<Response> singIn(String username, String password) {
    return ProjectClient.instance.dio.post('/api/Account/Login',
        data: {'Username': username, 'Password': password});
  }
}
