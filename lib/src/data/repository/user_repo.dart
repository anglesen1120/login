import 'dart:async';
import 'package:dio/dio.dart';
import 'package:app_salemanager_project/src/data/remote/user_services.dart';
import 'package:app_salemanager_project/src/data/spref/spref.dart';
import 'package:app_salemanager_project/src/shared/helpper/sprefcache.dart';
import 'package:app_salemanager_project/src/shared/model/User.dart';
import 'package:flutter/material.dart';

class UserRepository {
  UserServices _userServices;

  UserRepository({@required UserServices userServices})
      : _userServices = userServices;

  Future<User> logIn(String username, String password) async {
    var complete = Completer<User>();
    try {
      var reponse = await _userServices.singIn(username, password);
      var userData = User.fromJson(reponse.data['Data']);
      if (userData != null) {
        SharedPreFerences.instance.set(SPrefCache.KEY_USER, userData.token);
        complete.complete(userData);
      }
    } on DioError catch (error) {
      print (error.response.data);
      complete.completeError("Đăng nhập thất bại");
    } catch (error) {complete.completeError(error);}

    return complete.future;
  }
}
