
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreFerences {
  static final SharedPreFerences instance = SharedPreFerences._internal();
  SharedPreFerences._internal();


  Future set (String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, value);
  }

  dynamic get (String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.get(key);
  }
}