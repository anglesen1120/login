
import 'package:app_salemanager_project/src/module/login_screen/login_screens.dart';
import 'package:app_salemanager_project/src/shared/helpper/constants_page.dart';
import 'package:flutter/material.dart';
import 'package:app_salemanager_project/src/module/flash_screen/flash_creens.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
//      theme: ThemeData(
//
//        primarySwatch: Colors.orange[200],
//      ),
      routes: <String, WidgetBuilder>{
        LOGIN: (BuildContext context) => LoginPage(),

        SPLASH_SCREEN : (BuildContext context) => AnimatedSplashScreen(),
      },
      initialRoute: SPLASH_SCREEN,
      home: LoginPage(),
    );
  }
}


